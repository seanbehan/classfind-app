package io.seanbehan.classfind

import android.os.AsyncTask
import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import java.io.IOException
import java.io.InputStream
import java.net.URL

class ScrapeTask(url: String) : AsyncTask<Void, Int, List<String>>() {
    private val scrapeURL: String = url
    var schoolNames: List<String> = listOf("carleton", "guelph", "ottawa", "queens", "toronto", "trent", "western")

    override fun doInBackground(vararg params: Void?): List<String> {
        val index: InputStream = fetchSite(scrapeURL)
        this.schoolNames = parseSite(index)
        return this.schoolNames
    }

    private fun parseSite(stream: InputStream): List<String> {
        // TODO: Finish writing XML parser
        val myParser: XmlPullParser = Xml.newPullParser()
        myParser.setInput(stream, null)

        return emptyList()
    }

    private fun fetchSite(url: String): InputStream {
        // TODO: Clean up these comments after the code works
        try {
            // val stream = URL(this.scrapeURL).openStream()
            return URL(url).openStream()
            // return stream.bufferedReader().use { it.readText() }
            // download and return the index file
        }
        catch (ioe: IOException) {
            ioe.printStackTrace()
        }
        throw error("failed to fetch site")
    }
}