package io.seanbehan.classfind

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // -------- Pseudo-code ---------
        // display default list of schools
        // update list of schools
        // if list is updated:
        //     replace list
        //     refresh view

        val myScraper = ScrapeTask("http://classfind.com")
        val index: List<String> = myScraper.execute().get()
        debug_text.text = index.toString()
    }
}
